FROM python:3.12-slim

WORKDIR /app

COPY pyproject.toml ./
COPY pixi.toml ./

RUN apt-get update
RUN apt-get -y install curl
RUN PIXI_VERSION=v0.19.1 curl -fsSL https://pixi.sh/install.sh | bash

COPY src/main.py ./src/
COPY entrypoint.sh ./

RUN chmod +x ./src/main.py
RUN chmod +x entrypoint.sh
RUN ln -s ~/.pixi/bin/pixi /usr/local/bin/pixi

ENTRYPOINT [ "/bin/bash" ]
CMD ["/app/entrypoint.sh"]
