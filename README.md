
# Запуск проекта
Собираем докер образ

`docker build . -t ods-mlops-project`

Запускаем его

`docker run ods-mlops-project`


# Использование линтеров
Ruff автоматически прогоняется при коммите и исправляет ошибки. Для запуска вручную можно использовать
`pixi run --environment dev ruff check --fix .`

# Виртуальное окружение:

`pixi shell`
